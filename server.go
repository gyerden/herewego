package main

import (
	"log"
	"net/http"

	"bitbucket.org/gyerden/herewego/webutils"
)

func main() {

	t := webutils.Tpl{
		Title:       "Home Page",
		Header:      "This is the page I call Home",
		Img:         "https://cdn.dribbble.com/users/26059/screenshots/4538274/marvelbabies.jpg",
		Description: "Go Baby Hulk Goooooo!!!!",
	}

	h := webutils.Handler{
		Name:        "Home Page Handler",
		Description: "This handler handles the home page like a boss",
		Action: func(w http.ResponseWriter, r *http.Request) {
			t.RenderTpl(w)
		},
		APIAction: func(w http.ResponseWriter, r *http.Request) {
			t.RenderJSON(w)
		},
	}

	http.FileServer(http.Dir("dist"))
	http.HandleFunc("/", h.Action)
	http.HandleFunc("/api/v1/templates", h.APIAction)

	log.Println("Listening on port 4000...")
	log.Fatal(http.ListenAndServe(":4000", nil))
}
