package webutils

import (
	"net/http"
)

// Handler : Overkill by design to illustrate OO in Golang
type Handler struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Action      func(http.ResponseWriter, *http.Request)
	APIAction   func(http.ResponseWriter, *http.Request)
}

// Handle : Again overkill to illustrate a method that returns a function
func (h Handler) Handle() func(http.ResponseWriter, *http.Request) {
	return h.Action
}
