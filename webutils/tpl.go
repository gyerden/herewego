package webutils

import (
	"encoding/json"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
)

// Tpl : Enhancing Template functionality
type Tpl struct {
	Title       string `json:"title"`
	Header      string `json:"header"`
	Img         string `json:"img"`
	Description string `json:"description"`
}

// RenderTpl : looks at index.html by default used by Handler.Action
func (t Tpl) RenderTpl(w http.ResponseWriter) {
	dat, err := ioutil.ReadFile("dist/index.gohtml")
	if err != nil {
		log.Fatal(err)
	}

	tpl, err := template.New("tpl").Parse(string(dat))
	if err != nil {
		log.Fatal(err)
	}

	tpl.Execute(w, t)
}

// RenderJSON : used for Handler.APIAction
func (t Tpl) RenderJSON(w http.ResponseWriter) {
	v, err := json.Marshal(t)
	if err != nil {
		log.Fatal(err)
	}
	tpl, err := template.New("tpl").Parse(string(v))
	if err != nil {
		log.Fatal(err)
	}

	tpl.Execute(w, t)
}
