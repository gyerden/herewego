## Here We Go

You wanted some code so I figured I'd write something up real quick. (this file was edited in VI by the way lol)

So I'm going to do something kind of interesting... We are going to do some Golang Web Dev.. its go time (this is the only time I've used "go" and pun is actually intended, ok thats not true) LETTTSSSS GOOOOOOOOOOOOOOOO!!!!! (k I'm done, promise...)


## Info

NOTE: Did not implement TDD for this mini project.

If you run the "server" executable you will have a server running on port 4000. There are two paths.

```
localhost:4000/
localhost:4000/api/v1/templates --JSON output of template metadata
```

I created a simple "webutils" package that expands on html/template and net/http. I created my own Handler and Template(Tpl) Struct and then created a method for each of those user defined types.

I wanted to hit on the question you all asked about "Object Oriented" go. This should be illustrated, in a basic way, in the above package. 

If you guys want me to enhance anything let me know, I figured this would be enough to get a dialogue started. 

Cheers!!